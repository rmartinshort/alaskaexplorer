#!/usr/bin/env python 

#########
#Functions for creating topographic and slab depth cross sections, and for finding associated earthquakes
#The aim for this is really to expore the positon of the subducting pacific plate beneath Alaska, and its relation of topography and 
#earthquake foci
#########

import os
import numpy as np
from obspy.fdsn import Client as fdsnClient
import datetime
from obspy import UTCDateTime



def getvolcanolocations():
	'''Opens the file of active volcano locations and returns their positions'''

	regioncoords = [-164.0,-120.0,53.5,72.0]
	infile = open('Datasets/Other/Active_volanoes.csv','r')
	lines = infile.readlines()

	print len(lines)

	vlons = []
	vlats = []
	for line in lines[2:]:
		vals = line.split(',')
		try:
			vlat = float(vals[4])
			vlon = float(vals[5])

			if ((regioncoords[0] < vlon < regioncoords[1]) and (regioncoords[2] < vlat < regioncoords[3])):
				vlons.append(vlon)
				vlats.append(vlat)
		except:
			continue

	return vlats,vlons

def getfaultpositions():
	'''Opens the file associated with the fault locations and returns ordered dictionaries of their locations'''

	infile = open('Datasets/Other/Alaska_faults.dat')
	lines = infile.readlines()
	infile.close()

	lats = []
	lons = []

	sublats = []
	sublons = []

	for line in lines[1:]:
		vals = line.split()
		if vals[0].strip() == '>':
			lats.append(sublats)
			lons.append(sublons)
			sublons = []
			sublats = []
		else:
			lon = float(vals[0])
			lat = float(vals[1])
			sublons.append(lon)
			sublats.append(lat)

	return lats,lons 



def getseismometerocations():

	client = fdsnClient('IRIS')

	starttime = '2013-01-01'
	endtime = str(datetime.datetime.today()).split(' ')[0]

	#region of interest: can change if desired
	regioncoords = [-171.0,-123.0,53.5,72.0]

	if os.path.isfile('Datasets/Seismometers/TAstations.dat'):
		print 'Found TA station data: No need to redownload'
	else:
		outfile = open('Datasets/Seismometers/TAstations.dat','w')
		inventoryTA = client.get_stations(network='TA',station='*',level='station',starttime=starttime,endtime=endtime,minlongitude=regioncoords[0],minlatitude=regioncoords[2],maxlongitude=regioncoords[1],maxlatitude=regioncoords[3])
		for entry in inventoryTA[0]:
			print entry
			lon = float(entry.longitude)
			lat = float(entry.latitude)
			outfile.write('%g %g\n' %(lon,lat))
		outfile.close()

	if os.path.isfile('Datasets/Seismometers/AKstations.dat'):
		print 'Found AK station data: no need to redownload'

	else:
		outfile = open('Datasets/Seismometers/AKstations.dat','w')
		inventoryAK = client.get_stations(network='AK',station='*',level='station',starttime=starttime,endtime=endtime,minlongitude=regioncoords[0],minlatitude=regioncoords[2],maxlongitude=regioncoords[1],maxlatitude=regioncoords[3])
		for entry in inventoryAK[0]:
			print entry
			lon = float(entry.longitude)
			lat = float(entry.latitude)
			outfile.write('%g %g\n' %(lon,lat))
		outfile.close()

	#open the TA station file, extract the coordinates and plot the data
	infile = open('Darasets/Seismometers/TAstations.dat','r')
	lines = infile.readlines()
	infile.close()
	latsTA = []
	lonsTA = []
	for line in lines:
		vals= line.split(' ')
		lonsTA.append(float(vals[0]))
		latsTA.append(float(vals[1]))


	#open the AK station file, extract the coordinates and plot the data
	infile = open('Datasets/Seismometers/AKstations.dat','r')
	lines = infile.readlines()
	infile.close()
	latsAK = []
	lonsAK = []
	for line in lines:
		vals= line.split(' ')
		lonsAK.append(float(vals[0]))
		latsAK.append(float(vals[1]))

	return lonsTA,lonsAK,latsTA,latsAK



def calculate_profiles(startcoors,endcoors):
	'''Determine the slap file that the user has asked for, and return the name of the netCDF

	This function comes from the section plotter in SlabViewer

	'''

	slabbounds = {}
	slabbounds['Alaska'] = [167,216,50,65,'Datasets/NetCDF/alu_slab1.0_clip.grd']


	#NOTE! Corrds should be entered in degrees W of 0 degrees N, otherwise things will go wrong!

	startlon = 360 + float(startcoors[0])
	startlat = float(startcoors[1])
	endlon = 360 + float(endcoors[0])
	endlat = float(endcoors[1])

	print 'Profile:'
	print 'Startcoors: %g %g' %(startlon,startlat)
	print 'Endcoors: %g %g' %(endlon,endlat)

	for element in slabbounds:
		boxbounds = slabbounds[element]
		lonmin = float(boxbounds[0])
		lonmax = float(boxbounds[1])
		latmin = float(boxbounds[2])
		latmax = float(boxbounds[3])
		grdfile = boxbounds[4].strip()

		#check if either the start or end coordinates are within the bounding box

		if (((lonmin < startlon < lonmax) and (latmin < startlat < latmax)) or ((lonmin < endlon < lonmax) and (latmin < endlat < latmax))):
			print 'Detected that we want to plot a topo section for slab %s' %(element)
			print 'Will be extracting data from file %s' %grdfile 

			Xdistances, Yslabdepths = makeslabdepthsectionplot([startlon,startlat],[endlon,endlat],grdfile,interp=False)
			Xdistances_topo, Yheights_topo = maketoposection([startlon,startlat],[endlon,endlat])

		else:
			print 'Your specified profile did not include a slab: just making a topo profile'

			Xdistances = None
			Yslabdepths = None

			Xdistances_topo, Yheights_topo = maketoposection([startlon,startlat],[endlon,endlat])


	return Xdistances, Yslabdepths, Xdistances_topo, Yheights_topo



def get_quakes(startcoors,endcoors,minmag=4.0):
	'''Get earthquakes within a region around the start and end coordinates. These will be plotted on the section, with x distance'''

	client = fdsnClient('IRIS')

	boxHW = 0.5 #half width of box

	hdir = os.getcwd()
	os.chdir('Datasets/Earthquakes/')

	quakefile = open('quakedata.dat','w')

	startlon = startcoors[0] + boxHW
	startlat = startcoors[1] - boxHW

	endlon = endcoors[0] - boxHW
	endlat = endcoors[1] + boxHW

	minlon = min(startlon,endlon)
	maxlon = max(startlon,endlon)

	minlat = min(startlat,endlat)
	maxlat = max(startlat,endlat)

	starttime = '1970-01-01'
	endtime = str(datetime.datetime.today()).split(' ')[0]

	quakecat = client.get_events(starttime=UTCDateTime(starttime), endtime=UTCDateTime(endtime), minlongitude=minlon, maxlongitude=maxlon, minlatitude=minlat, maxlatitude=maxlat, minmagnitude=minmag)

	for event in quakecat:

		evlon = event.origins[0].longitude
		evlat = event.origins[0].latitude
		evdep = event.origins[0].depth

		quakefile.write('%s %s %s\n' %(evlon,evlat,evdep))

	quakefile.close()

	#Work out the distance from each quake to the profile line, and write to a file 

	#Make a file containing quakelon, quakelat, dist, and dist along profile
	os.system('gmt mapproject quakedata.dat -Lgridsectiontopo.dat/k > quake_dist.dat')

	#Reorder the columns and do another grdtrack to get distance along the profile
	os.system("awk '{print $5,$6,$1,$2,$3,$4}' quake_dist.dat > quaketmp.dat")
	os.system("rm quake_dist.dat")

	#Now, calculate distance along the profile from the start point
	os.system('gmt mapproject quaketmp.dat -G%s/%s/k > quake_points.dat' %(str(startcoors[0]),str(startcoors[1])))
	os.system('rm quaketmp.dat')

	#Now, open the newly created file and grid section file, and pull the distance data
	infile1 = open('quake_points.dat','r')
	lines1 = infile1.readlines()
	infile1.close()

	Xdistances_quakes = []
	Ydepths_quakes = []
	evlons = []
	evlats = []

	for line in lines1:
		vals = line.split(' ')
		try:

			evlon = float(vals[0].split('\t')[-1])
			evlat = float(vals[1])
			evdep = float(vals[2])
			evdist = float(vals[3].split('\t')[-2])
			evdistalong = float(vals[3].split('\t')[-1])

			#Only keep if the distance between the event and the profile line is less then 50km
			if evdist <= 50:
				Xdistances_quakes.append(evdistalong)
				Ydepths_quakes.append(-evdep)
				evlons.append(evlon)
				evlats.append(evlat)

			#for some reason, some depths don't exist, so use this try; except statement
		except:
			continue

	os.chdir(hdir)

	return Xdistances_quakes, Ydepths_quakes, evlons, evlats



def makeslabdepthsectionplot(startcoors,endcoors,filename,interp=True,test=None):
	'''Make a slab cross section plot from netcdf data'''

	hdir = os.getcwd()
	os.chdir('Datasets/Earthquakes/')

	startlon = startcoors[0]
	startlat = startcoors[1]

	endlon = endcoors[0]
	endlat = endcoors[1]

	sectionname = 'section.dat'

	#Create the section coordinates. Should make some sort of auto-decision about the spacing
	os.system('gmt project -C%s/%s -E%s/%s -G10 -Q > %s' %(startlon,startlat,endlon,endlat,sectionname))
	os.system('gmt grdtrack -Rg %s -G%s > gridsection.dat' %(sectionname,filename))

	os.system('rm section.dat')

	sectionfile = open('gridsection.dat','r')
	lines=sectionfile.readlines()
	sectionfile.close()

	Xdistances = []
	Ydepths = []

	for line in lines:
		vals = line.strip().split('\t')
		depthvalue = vals[-1]
		if depthvalue.strip() != 'NaN':
			distance = float(vals[-2])
			slabdepth = float(vals[-1])
			Xdistances.append(distance)
			Ydepths.append(slabdepth)

	if test:
		#test to see if the data is any good
		if ((len(Xdistances) > 2) and (len(Ydepths) > 2)):
			return True
		else:
			print Xdistances, Ydepths
			print 'A problem occured!'
			return False

	if interp:

		#Calculate polynomial for fitting. Third order is a arbitary choice
		z = np.polyfit(array(Xdistances),array(Ydepths),3)
		f = np.poly1d(z)

		Xfitted = linspace(min(Xdistances),max(Xdistances)+100,1000) #extrapolate some distance further from the slab edge (to be determined)
		Depthfitted = f(Xfitted)

		os.chdir(hdir)

		return Xdistances, Ydepths, Xfitted, Deptfitted

	else:

		os.chdir(hdir)

		return Xdistances, Ydepths

def makecrustsection(startcoors,endcoors):
	'''Create a crustal thickness section through the CRUST 1.0 model'''

	crustfile = 'Datasets/NetCDF/crustal_globe_xyz.nc'

	startlon = startcoors[0]
	startlat = startcoors[1]

	endlon = endcoors[0]
	endlat = endcoors[1]

	sectionname = 'sectioncrust.dat'

	#Create the section coordinates. Should make some sort of auto-decision about the spacing
	os.system('gmt project -C%s/%s -E%s/%s -G10 -Q > %s' %(startlon,startlat,endlon,endlat,sectionname))
	os.system('gmt grdtrack %s -G%s > gridsectioncrust.dat' %(sectionname,crustfile))

	os.system('rm sectioncrust.dat')

	sectionfile = open('gridsectioncrust.dat','r')
	lines=sectionfile.readlines()
	sectionfile.close()

	Xcrustthick = []
	Ycrustthick = []

	for line in lines:
		vals = line.strip().split('\t')
		depthvalue = vals[-1]
		if depthvalue.strip() != 'NaN':
			distance = float(vals[-2])
			crustthick = -float(vals[-1])
			Xcrustthick.append(distance)
			Ycrustthick.append(crustthick)

	return Xcrustthick, Ycrustthick


def maketoposection(startcoors,endcoors):
	'''Create a simple topographic section using GEBCO_08 global topography'''

	topofile = '/Users/rmartinshort/Documents/NCmaps_topo_tomo/gebco_08.nc'

	startlon = startcoors[0]
	startlat = startcoors[1]

	endlon = endcoors[0]
	endlat = endcoors[1]

	sectionname = 'section.dat'

	#Create the section coordinates. Should make some sort of auto-decision about the spacing
	os.system('gmt project -C%s/%s -E%s/%s -G10 -Q > %s' %(startlon,startlat,endlon,endlat,sectionname))
	os.system('gmt grdtrack %s -G%s > gridsectiontopo.dat' %(sectionname,topofile))

	os.system('rm section.dat')

	sectionfile = open('gridsectiontopo.dat','r')
	lines=sectionfile.readlines()
	sectionfile.close()

	Xdistances = []
	Yheights = []

	for line in lines:
		vals = line.strip().split('\t')
		depthvalue = vals[-1]
		if depthvalue.strip() != 'NaN':
			distance = float(vals[-2])
			slabdepth = float(vals[-1])
			Xdistances.append(distance)
			Yheights.append(slabdepth)

	return Xdistances, Yheights

###################################
#Testing
##################################

if __name__ == '__main__':

	V1,V2 = getvolcanolocations()
	print V1 
	print V2