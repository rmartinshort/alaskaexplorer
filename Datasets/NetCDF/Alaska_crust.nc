CDF       
      lon    7   lat             Conventions       COARDS, CF-1.5     title         Alaska_crust.nc    history       �grdsample /Users/rmartinshort/Documents/Berkeley/Alaska/Analysis_tools/Alaska_section_plotter/crustal_globe_xyz.nc -R-180/-115/47/70 -GAlaska_crust.nc     GMT_version       5.1.1 (r12968) [64-bit]          lon                 	long_name         	longitude      units         degrees_east   actual_range      �f�     �\�          �     lat                	long_name         latitude   units         degrees_north      actual_range      @G�     @Q�           �  �   z                      	long_name         z      
_FillValue        �     actual_range      @��   @L��         0  `�f�     �fY{B^И�f2����/�fq�q��e��	{B_�e�hK���e��8��er^З�&�eK�����e%UUUUU�d�З�%��d�K����d��q��d�B^З��dd��/hL�d>8�8��d�%�	{�c�/hK��cʪ�����c�%�	{B�c}�/hK��cWq�r�c0��%�
�c
�����b�8�9�b�	{B^��b����/h�bp     �bI{B^И�b"����0�a�q�q��a��	{B_�a�hK���a��8��ab^З�&�a;�����aUUUUU�`�З�%��`�K����`��q��`{B^З��`T��/hL�`.8�8��`�%�	{�_�^З�&�_uUUUUV�_(K����^�B^З��^�8�8��^A/hK��]�%�	{B�]�q�r�]Z�����]	{B^��\�     @G�     @H򆼡�@H��yC^@IPה5�@I���@J����(l@K!�(k�@K���(k�@LW�5�y@L򆼡�(@M�yC^P�@N(k��@N�^Pה6@O^Pה5�@O�C^Pה@PJ򆼢@P��5�y@P�yC^Q@Q2����(@Q�     @�I�@ⶎ@��s@�2�@�`@㬭@�Õ@�_@��Y@�q$@�6@�x@�0�@�Jd@��@�5@�l@�V@�".@�O�@�|J@��L@ޫ�@���@��;@��@�z@�&�@�ô@��)@�0[@��@���@߮�@� @߻�@�Q�@�;�@���@�K�@�aA�A@�aA[�AJ80A�A��B.ĬB-Bk�B
��B�@B��B
�@�9�@���@���@��1@⢮@�k�@�2@�\@�*@�Yq@�y�@��^@���@�B@�)�@��I@�۳@�(@�ɍ@�7|@�)b@�=@�/@߻�@�t@߸@��@�X@ރ%@ާ+@��@ބ�@��z@��o@�"@�&@݈
@�"@ވ~@�gP@�a�A�MA$�A[�CA��3A��A�<bB�B/#B�B�A��A�'CBv�B��@�W@�C;@��~@��@�T1@���@��;@�^@�u@�s@ޖ�@݃r@�sA ��@�;|@��@܈�@���@߭�@�x@��8@�l�@ܱ%@��@��@���@��@���@���@���@އy@�@���@�ˍ@�Q�@�� @݀�@�`@ܯ�@ܧ(@��pA��A%TQA� 9Aθ^B&zyB�B*BKB�RB�B�UB��BҮBHUA�A""�AD�A��A%�@���@�@�pq@�!p@��@�bD@��h@��F@��/@���@���@��.@�&u@���@� @�_�@�>�@��@�0�@ݲ<@�.@��@�j@ތ�@��:@��@�*2@�rl@ߨ@�_�@�!�@�Z@�|v@�-b@�lXAbA"�^A|�RA��bA���BFbB�>BP4Br�B�B��B	XBFB+�KB:^�A���A�yA�~,A��AW��A^ΆAb�#AV]6AT�7A!��A�@� �@��@���@��/@�f'@��G@�,�@ݢ9A P�@��~@�^�@�Z@��@␯@���@�,�@��@�-�@�=?@�D�@��U@��@��@咕A�@�.[@��)@�2A'�ASaA��A��A�^�B
JB��BN�B�B��B
vB��B$�B��Bf6XB@�{A�k�A\A,�A)gA)�aA�/�A���A��UA���A�6{AygQA\��A*v�A&�!A�]A�>A�A��@� �@�H�@�'�@���@�T@ߎ�@�t+@ޗ9@�s�@گ�@ڕ�@�D�@�8@�{aA��A^pA<�AsO@��@�4�AcvA*]�A��A�.�A���A���A�q�B��BH�B s�B��A���B!��BJB�B=�BC�B;5�Am�UA4aTAFv>ABv*A>��A�}A��A��A~iA���Aɨ�A��A��A��{A���A�i$A�բA��lA�@�AT��A0�A�_@��@�?D@��O@ᣘ@�r�@��@�Q�@�h�@�7A �A �@�.�A��A�wA��A��AS�tA��A�_�A�&A��A���B
�hB�UB%�B��B��Ba�B(X�B@�B;>�B%]�B��A-�AB:�A@��A3�A3�*A8keAI�ALHvA�$^A�g�AჷA�<ZA�O�A���A�F�B B6�A���A�ڒA�VA�?A�l�A&��A'MNA	0U@�h@忮@��A�@���@���@�29AlA�yA��AJ,hA~i�A�޵A��#Aݓ�A겿BB j1B��B;YB)&B�>B�yBsdB&BS�Bg�B!rAB!�AB ��A.��A/�sA0�GA3�<AI��A�;MA��rA�B�A鈟A�s�A�zA���A��A��KB 
RA��pA���B ͕B �A��A��A��(A���A�1�Az� A#��@�{3@��5A Š@��@��6A  :@�h<A	�@��`A��A�/8A���AִA�ɭB	g=B ��BХB�B!CyBAM\B(B@]B.sJB+b�B��B��B2�B��B 8�A!q�A��AN�AT�ZA���A��RA���A��`A���A�dA�'�A��A��A�tA�'gA��[A��}A���A�w A���B��B/�A��KA�+�A�2�A��DA7� A��@��p@�>8@�M�@��A��A NA�Aʒ�A�C�A�m�Aҷ�A���B�{B��B
��B��B�B:ͺB=�B�,B-�B*�\B�|BsB&+:B0�.B4J3A;<iAT��A�y�A��6A�^A�,SA��A�wA�~�A�"�A�r,A�!qA��?A��A��A�4A���A�E�A��AA�hLB
 vBJ�B�B'��B$ A�kSA�&;A��AG4]AD�UA9��ALqA��JA��iA�m{B�KB	��A���A��0B�~B!�B��Bh�B�SB�^B*�B,��B,�aB+@3B*�CB$X\B$�lB$M�B"��B#s�A�vNA��zA��hA��A��A�/A�z�A��A�A���A��A�C�A�vA�6WA�KA�L(A�D�A�{nA��A���B;B��BodB�,B��B�BxB	6�B (�B��A�^UA�rjA��BCpB*=B��B
FBNaB�BH�BL!B'�B'+gB&L.B%�<B!�3B'TYB'�B'�B&��B"llB&��B(SfB$�JB%OpAҁ�A���A�SA��A�RBA�v\A��A�A羽A�R�A�q�A�K�A�7�A��cA���A��=A��A��!A��VA�xB#B��A��IA�85A�l�B{?B'
�B#B#��B#��B�A��tA�U�A���B �AB
�B�B6MB8B1:B~�B-^�B,��B,�&B*��B(�B"<bB!��B!�sB!�BB%�]B#�8B cXB�>A�L�A��AA��A�A斓A쩦A�R/A�GLA�H�A� A��VA��eA��VA���A��kA� A�  A���A���B�_B
�BEQB�tB=�B�rB�eB0�B-�B7 �B!��B4�B�pB�B�B	��B�4B��B  B�tBK�B,jwB+��B+��B*/�B�B(�B7B{
B��Bi�B�>B�eB ��B�QA��A��gA���A�LA�dA�"�Bp�B #�A�z�A��SA�<�A��AA���A��A��A��DA�D�A�IA�@A�B%�BU�Bw�B�B\UB�B�B�|BK�B�B�:Be�BoB	T�B	��B	��B0�BTBT�B�KB��B0��B/� B/͈B.LBR�B9�B�B �BܠB �B��B�tB
[�B-�B�B+�B/8B|B�?B*�BΏA���A�A䭆A���A�^�A��WA��lA�EYA�bA���A�e(A�]GA��B	��B	ЇB
a"BH�B	��B��A�\CA��RA��?B��B��B�eBoB�B��B �B��B�dB/�BEkB-�B4�+B3|XB3�'B1��B!�B"�B"B�9B6~BB"HB��BlB�fB��B
�<B
��B
�5B��B�GB��A���A�w�A��zA�5�A��
A�MsA�®BȥA�p�B@VB��B�B�B�B��B��BXB��B�B�B	��Bt�B%,B�B�FB�.B �B%+B�B�yB�&B�B�B�eB�BؼB�5B��B[KBN0BB�sB�B`�B3�BE�B�B�B�9BdHBo�B�B�B��A���A���A�!�A�TA��A�3�A�9A�j�B��Bn�B(dB��B��B��B��B��Bq�B�B �B$�aB�B,n�B�B�pB%BD�B�FB{�B��B��Bs�B'�B�FB�VB�eB�B9BBvB'BgBpDB��B�BB�'Bz�B��B��B*�B�zB��BX�B
�B:jA���A�jvA�WA�dA�2�A�J5A�#�A��cB
s�B�QB�YBI6BJ�BL0Bm{B!#B�B/LB,i�B6'2B1ٓBBӏB*�BKBvOB�BU�B�&B\By�B�2B��B��B:�B
��BH�Bj�B2*B	]Bx�B�bBm<B͆B��B��B��B w�B!�B%6B6�B��B`�A���A���A��|A���A�w�A�,A�l8A뼐A�g�A���A�Z;A�$�A��B%B�<B�B<B1pB��B��BG�B�uB$jB$D+B!H�B�QB&^B�BٟB�B��A���A���B�AB$GB
?�B	�B��B;B�NB�fBiYB��B:�BڼB�:B��B_CB#�B.�DB,
B#�