#!/usr/bin/env python
#RMS November 2015

#--------------------------------------
#Alaska dataset explorer GUI and companion functions
#--------------------------------------

print 'Importing modules ........'
from Tkinter import *
import tkFileDialog
import matplotlib 


#Allow matplotlib to be used within a tkinter canvas
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
import matplotlib.pyplot as plt

#Import functionality to make the slab/quake maps
import Quake_slab_map as QSmapper 

#Import basemap functionality and other things
from mpl_toolkits.basemap import Basemap, cm 
import numpy as np
import os
import datetime
from quitter import Quitter
from netCDF4 import Dataset
from obspy import UTCDateTime

print  'Done importing!'


#Control matplotlib font size
matplotlib.rcParams.update({'font.size': 6})


#Home directory
global myhome
myhome = os.getcwd()

print 'Setting up GUI ............'

############################################################
#Companion functions
############################################################


def downloadevents(t1,t2,regioncoords,minmagnitude=4.0,recent=False):
	'''Get events for the region, corresponing to the user's criteria'''

	from obspy.fdsn import Client as fdsnClient
	client = fdsnClient('IRIS')
	from obspy import UTCDateTime

	if recent:
		filename = myhome+'/Datasets/Earthquakes/Alaska_7day_seis.dat'
	else:
		filename = myhome+'/Datasets/Earthquakes/All_quakes_Alaska_M%g.dat' %float(minmagnitude)

	quakefile = open(filename,'w')

	minlon = regioncoords[0]
	maxlon = regioncoords[1]
	minlat = regioncoords[2]
	maxlat = regioncoords[3]

	starttime = t1
	endtime = t2
	minmag = float(minmagnitude)

	quakecat = client.get_events(starttime=UTCDateTime(starttime), endtime=UTCDateTime(endtime), minlongitude=minlon, maxlongitude=maxlon, minlatitude=minlat, maxlatitude=maxlat, minmagnitude=minmag)

	for event in quakecat:

		evlon = event.origins[0].longitude
		evlat = event.origins[0].latitude
		evdep = event.origins[0].depth

		quakefile.write('%s %s %s\n' %(evlon,evlat,evdep))

	quakefile.close()

	print 'Done downloading events of magnitude %g and higher!' %minmag



def maketomodepthslice(datasetname,depth):
	'''Make a map view slice through the tomography model (in .dat format) given by the variable datasetname. This file should be in the AlaskaExplorer directory'''

    #Earth radius
	Erad = 6371
	#Region of interest in GMT coordinates
	Rregion = '-Rd-175/-115/50/76'

	datasetpath = myhome+'/Datasets/Tomography/'+datasetname

	infile = open(datasetpath,'r')
	lines = infile.readlines()
	infile.close()

	tmpdepths = []
	#############################
	#Work out the depth we want to slice at - basically, we compare the input depth to all the depths in the data file, and work out which one is closest

	for line in lines[1:]:
		vals = line.split('   ')
		R = str(vals[0]).strip()
		lat= float(vals[1])
		lon = float(vals[2])
		tomoval = float(vals[3])
		if R in tmpdepths:
			break 
		else:
			tmpdepths.append(R)

	j = 0
	for val in tmpdepths:
		tmpdepths[j] = float(val);
		j += 1

	tmpdepths = np.array(tmpdepths)
	closedepths = np.zeros(len(tmpdepths));

	for i in range(len(tmpdepths)):
		closedepths[i] = abs(tmpdepths[i]-(Erad-depth))

	index = np.argmin(closedepths)
	slicedepth = tmpdepths[index]

	#############################
	#Now, write a netcdf file for that slice

	print 'Working with depth %s' %str(slicedepth)
	outfilename = myhome+'/Datasets/Tomography/depthslice_%s.dat' %str(slicedepth)
	ncfilename = outfilename[:-4]+'.nc'

	if os.path.isfile(ncfilename):
		print 'Found slice file: no need to recreate'

	else: 
		outfile = open(outfilename,'w')

		for line in lines[1:]:
			vals = line.split('   ')
			R = vals[0]
			if (float(R) == float(slicedepth)):
				lat = float(vals[1])
				lon = float(vals[2])

				#NOTE! Here we may invert the sign of the velocity perturbation so that the pyhton colorsceheme matches it. For the rdbu colorscheme, this isn't necessary
				tomoval = float(vals[3])
				outfile.write('%g %g %g\n' %(lon,lat,tomoval))

		outfile.close()

		os.system('gmt surface %s %s -T0.1 -I0.1/0.1 -G%s' %(outfilename,Rregion,ncfilename))
		os.system('rm %s' %outfilename)

		print 'Written %s' %ncfilename


	return ncfilename,str(slicedepth)



############################################################

class ExplorerGUI(Frame):
	'''Base class controlling GUI aspects of the program'''

	def __init__(self, parent, width=1100, height=400, **options):

		Frame.__init__(self,parent, **options)

		self.grid(sticky=E+W+N+S)

		top=self.winfo_toplevel()

		for i in range(10):
			top.rowconfigure(i,weight=4)
			self.rowconfigure(i,weight=4)
		for i in range(2):
			top.columnconfigure(i,weight=1)
			self.columnconfigure(i,weight=1)

		self.f = Figure(figsize=(6.5,6.5),dpi=150,facecolor='white')
		self.a = self.f.add_subplot(111)

        #------------------------------------------------------------------
		#Create the Basemap instance - the map that the user interacts with
		#------------------------------------------------------------------

        #Tell basemap to use the axis ax, and draw the details
		self.m = Basemap(ax=self.a,width=3000000,height=2600000,resolution='l',projection='aea',lat_1=54.0,lat_2=69.0,lon_0=-146,lat_0=61)
		self.m.shadedrelief()

		#Can choose to 
		#m.drawmapboundary(fill_color='blue')
		#m.fillcontinents(color='green',lake_color='blue')
		#m.drawcountries()

		p1 = np.arange(-90.,91.,3.)
		m1 = np.arange(-180.,181.,5.)
		self.m.drawparallels(p1,labels=[False,True,False,False])
		self.m.drawmeridians(m1,labels=[False,False,False,True])

		self.canvas = FigureCanvasTkAgg(self.f, self)
		self.canvas.show()
		self.canvas.get_tk_widget().grid(row=0,sticky=W+S+N+E,columnspan=8,rowspan=9)

        #Create the menu bar
		self.Createmenubar(parent)

		#set some display parameters
		self.displayseismometers = False
		self.displayquakes = False
		self.displayvolcanoes = False
		self.alreadynetcdf = False
		self.displayrecent = False
		self.displayfaults = False
		self.faults = []

		#No colorbars are presnent on startup
		self.cbar = False

		#The tomography slice window is currently closed
		self.Twindowopen = False

		#Set up dictionary with the user entries
		self.userentries = {}

		#Display the title
		Label(self,text='Alaska Data Explorer V1.0',bg='azure',height=4,pady=2,font='Helvetica 16 bold').grid(row=0,column=9,columnspan=3,sticky=W+E+S+N)

		#Display the main user entry boxes

		#Box 1: Earthquake display options
		Label(self, text='Earthquake-related options',bg='powder blue',height=3,font='Helvetica 12 bold').grid(row=1,column=9,columnspan=3,sticky=W+E)
		Label(self, text='Section [lon1/lat1/lon2/lat2]',pady=1).grid(row=2,column=9,sticky=W)
		e1 = Entry(self)
		e1.grid(row=2,column=10,sticky=W+E)
		Button(self, text='Plot section',pady=1,padx=1,command=self.QuakeSlabSection).grid(row=2,column=11,sticky=W)

		#Append user entries to the dictionary of entries
		self.userentries['Quakesection'] = e1

		Label(self, text='Display all quakes > M',pady=1).grid(row=3,column=9,sticky=W)
		e3 = Entry(self)
		e3.grid(row=3,column=10,sticky=W)
		Button(self, text='Map quakes',pady=1,padx=1,command=self.DisplayQuakes).grid(row=3,column=11,sticky=W)

		#Append the minumum magnitude to dictionary
		self.userentries['Mapquakesmag'] = e3

		#Zoom label
		Label(self, text='Zoom options',bg='powder blue',height=3,font='Helvetica 12 bold').grid(row=4,column=9,columnspan=3,sticky=W+E)
		Label(self, text='Zoom window',pady=1).grid(row=5,column=9,sticky=W)
		e4 = Entry(self)
		e4.grid(row=5,column=10,sticky=W)
		Button(self, text='Zoom [minlon/maxlon/minlat/maxlat]',pady=1,padx=1,command=self.Zoommap).grid(row=5,column=11,sticky=W)

		#Append the minumum magnitude to dictionary
		self.userentries['Zoomwindow'] = e4

		Button(self, text='Reset map',pady=1,padx=1,command=self.Resetmap).grid(row=6,column=11,sticky=W)
		Button(self, text='Reset form',pady=1,padx=1,command=self.Resetform).grid(row=6,column=10,sticky=W)

		Quitter(self).grid(row=7,column=9,sticky=E)

		#User this button to remove existing datasets and fetch new ones when requested
		Button(self, text='Refresh datasets',pady=1,padx=1,command=self.Deleteseis).grid(row=7,column=11,sticky=W)

		parent.title("AlaskaExplorer")

	def Zoommap(self):
		'''Zoom into the main map with the requested boundary coordinates'''

		#Note that this overrides any displayed netCDF datasets, but these can be redrawn
		self.alreadynetcdf = False;

		print 'Zoom in'

		zoombounds = self.userentries['Zoomwindow'].get()

		vals = zoombounds.split('/')
		minlon = float(vals[0])
		maxlon = float(vals[1])
		minlat = float(vals[2])
		maxlat = float(vals[3])

		#Make the new map  - zoomed box

		self.f = Figure(figsize=(6.5,6.5),dpi=100,facecolor='white')
		self.a = self.f.add_subplot(111)
		self.m = Basemap(ax = self.a, llcrnrlon=minlon, llcrnrlat=minlat, urcrnrlon=maxlon, urcrnrlat=maxlat, projection='aea', resolution='h',lat_1=54.0,lat_2=69.0,lon_0=-146,lat_0=61)

		p1 = np.arange(minlat,maxlat,1.)
		m1 = np.arange(minlon,maxlon,2.)
		self.m.drawparallels(p1,labels=[False,True,False,False])
		self.m.drawmeridians(m1,labels=[False,False,False,True])

		self.m.shadedrelief()

		#m.drawmapboundary(fill_color='white')
		#m.fillcontinents(color='black',lake_color='white')
		self.m.drawcountries()


		#Overwrite the existing box with the new map
		#self.canvas = FigureCanvasTkAgg(self.f, self)
		self.canvas.draw()
		#self.canvas.get_tk_widget().grid(row=0,sticky=W+S+N+E,columnspan=8,rowspan=9)

	def DisplayYak(self):
		'''Display the extent of the subducted Yakutat Terrain, as inferred by Finzel et al (2011)'''

		infile = open(myhome+'/Datasets/Other/Yakutat_subducted_extent.dat','r')
		lines = infile.readlines()
		infile.close()

		pointlons = []
		pointlats = []

		for line in lines:
			vals = line.split('  ')
			lon = float(vals[0])
			lat = float(vals[1])
			pointlons.append(lon)
			pointlats.append(lat)

		xevent,yevent = self.m(pointlons,pointlats)
		self.m.plot(xevent,yevent,'k--',linewidth=2.5,alpha=0.9,label='Yakutat subduction extent')

		handles, labels = self.a.get_legend_handles_labels()
		self.a.legend(handles, labels, bbox_to_anchor=(0.2, 0.1), fancybox=True, shadow=True)

		self.canvas.draw()


	def Resetmap(self,quick=False):
		'''Redraw the original map: Returns user to Alaska overview mode. Redraw in quick mode if we're overaying a dataset'''

		print 'Refreshing map..'

		self.f = Figure(figsize=(5,5),dpi=150,facecolor='white')
		self.a = self.f.add_subplot(111)

		self.m = Basemap(ax=self.a,width=3000000,height=2600000,resolution='l',projection='aea',lat_1=54.0,lat_2=69.0,lon_0=-146,lat_0=61)

		if quick == False:
			self.m.shadedrelief()

		p1 = np.arange(-90.,91.,3.)
		m1 = np.arange(-180.,181.,5.)
		self.m.drawparallels(p1,labels=[False,True,False,False])
		self.m.drawmeridians(m1,labels=[False,False,False,True])

		self.canvas = FigureCanvasTkAgg(self.f, self)
		self.canvas.show()
		self.canvas.get_tk_widget().grid(row=0,sticky=W+S+N+E,columnspan=8,rowspan=9)

        #Reset all the display variables
		self.displayquakes = False
		self.displaycrustalthickness = False
		self.displayseismometers = False
		self.displayvolcanoes = False
		self.displaytopo = False
		self.displayrecent = False

	def DisplayRecentQuakes(self):
		'''Get just the last 7 days of seismicity'''

		if self.displayrecent:

			self.recentquakes.pop(0).remove()

		print 'Getting last 7 daya worth of events for Alaska. M 2.0 +'

		today = UTCDateTime(str(datetime.datetime.now()))
		starttime = today - (7*24*3600) #Count backwards 7 days

		regioncoords = [-171.0,-123.0,48.5,72.0] #For Alaska
		downloadevents(starttime,today,regioncoords,minmagnitude=2.0,recent=True) #Get bascially all the recent seismicity

		inputfilename = myhome+'/Datasets/Earthquakes/Alaska_7day_seis.dat'
		quakefilein = open(inputfilename,'r')
		lines = quakefilein.readlines()
		quakefilein.close()

		alllons = []
		alllats = []

		for line in lines:
			vals = line.split(' ')
			lon = float(vals[0])
			lat = float(vals[1])
			alllons.append(lon)
			alllats.append(lat)

		xevent,yevent = self.m(alllons,alllats)
		self.recentquakes = self.m.plot(xevent,yevent,'g*',label='Last 7 days',markersize=4.0,alpha=0.6)

		handles, labels = self.a.get_legend_handles_labels()
		self.a.legend(handles, labels,loc='upper center', bbox_to_anchor=(0.5, 1.1), ncol=2, fancybox=True, shadow=True, fontsize=6.0)

		self.canvas.draw()

		self.displayrecent = True


	def DisplayQuakes(self):
		'''Update the map with earthquakes of a particular magnitude and above'''

		if self.displayquakes:
			print 'Quakes already displayed: reset map before you plot again'
		else:

			starttime = '1970-01-01'
			endtime = str(datetime.datetime.today()).split(' ')[0]
			mag = self.userentries['Mapquakesmag'].get()

			#colorscheme for earthquakes 
			quakecolors=['k.','b.','r.','c.','m.']

			quakefilename = myhome+'/Datasets/Earthquakes/All_quakes_Alaska_M%s.dat' %mag.strip()

			#Search for the quakes file: Don't download it twice if it exists, because this takes a long time
			if os.path.isfile(quakefilename):
				print 'Found Alaska quakes file: no need to download'
			else:
				regioncoords = [-171.0,-123.0,48.5,72.0]
				downloadevents(starttime,endtime,regioncoords,minmagnitude=mag)

			#Get the data and plot the quakes

			quakefilein = open(quakefilename,'r')
			lines = quakefilein.readlines()
			quakefilein.close()

			alllons = []
			alllats = []
			alldepths = []

			for line in lines:
				vals = line.split(' ')
				depth = vals[2].strip()
				if depth != 'None':
					lon = float(vals[0])
					lat = float(vals[1])
					depth = float(depth)/1000.0 #convert to meters 
					alllons.append(lon)
					alllats.append(lat)
					alldepths.append(depth)

			quakedepthbins = {}
			quakedepthbins['Shallow: < 20km'] = [0.5,20]
			quakedepthbins['Intermediate: 20-60km'] = [20,60]
			quakedepthbins['Deep: 60-150km'] = [60,150]
			quakedepthbins['V. Deep: 150-300km'] = [150,300]
			quakedepthbins['Below 300km'] = [300,1000]

			i=0
			for depthbin in quakedepthbins:
				mindepth = quakedepthbins[depthbin][0]
				maxdepth = quakedepthbins[depthbin][1]

				londepth = []
				latdepth = []

				for element in zip(alllons,alllats,alldepths):

					if (mindepth < element[2] < maxdepth):
						londepth.append(element[0])
						latdepth.append(element[1])

				xevent,yevent = self.m(londepth,latdepth)
				self.m.plot(xevent,yevent,quakecolors[i],label=depthbin,markersize=2.0,alpha=0.5)
				i+=1

			handles, labels = self.a.get_legend_handles_labels()
			self.a.legend(handles, labels,loc='upper center', bbox_to_anchor=(0.5, 1.1), ncol=2, fancybox=True, shadow=True, fontsize=6.0)

			self.canvas.draw()

			self.displayquakes = True

	def QuakeSlabSection(self):
		'''Use functionality provided by the Quake slab map program to create maps of the slab at different points'''

		print 'Plotting slab map'

		import matplotlib.pyplot as plt

		sectioncoors = self.userentries['Quakesection'].get()

		vals = sectioncoors.split('/')
		lon1 = float(vals[0])
		lat1 = float(vals[1])
		lon2 = float(vals[2])
		lat2 = float(vals[3])

		#quakemag = self.userentries['Mapquakesmag'].get()

		startcoors = [lon1,lat1]
		endcoors = [lon2,lat2]

		lons = [lon1,lon2]
		lats = [lat1,lat2]

		#Append section to the inset map
		x,y = self.m(lons,lats)
		self.m.plot(x,y,'r--',linewidth=3)
		self.canvas = FigureCanvasTkAgg(self.f, self)
		self.canvas.show()
		self.canvas.get_tk_widget().grid(row=0,sticky=W+S+N+E,columnspan=8,rowspan=9)

		#Make slab profile
		Xslab, Yslab, Xprofile ,Yprofile = QSmapper.calculate_profiles(startcoors, endcoors)

		#Get earthquakes
		Xquake, Yquake, evlons, evlats = QSmapper.get_quakes(startcoors,endcoors)

		#Get crustal base profile
		Xcrust,Ycrust = QSmapper.makecrustsection(startcoors,endcoors)

		#Create separate (pop up) plot 
		fig1 = plt.figure() 
		ax1 = fig1.add_subplot(111)

		if Xslab != None:
			ax1.plot(np.array(Xslab),np.array(Yslab),'r--',linewidth=5,label='Slab 1.0')
			plt.hold('on')

		ax1.plot(np.array(Xprofile),np.array(Yprofile)/1000.0,'k-',linewidth=2,label='Topography')
		plt.hold('on')
		ax1.plot(np.array(Xcrust),np.array(Ycrust),'b-',linewidth=2,label='Crust 1.0')
		plt.hold('on')
		ax1.plot(np.array(Xquake),np.array(Yquake)/1000.0,'k.',label='Quakes')
		ax1.set_ylim([-200,5])

		plt.legend(loc='best')
		ax1.set_xlabel('Distance along selected profile [km]')
		ax1.set_ylabel('Surface height [km]')
		ax1.set_title('Topographic profile and quakes')
		plt.axis('equal')
		plt.grid()
		plt.show()

	def Displayseismometers(self):
		'''Display all seismometers in the region of interest'''

		if self.displayseismometers:
			
			self.AKstations.pop(0).remove()
			self.TAstations.pop(0).remove()

		lonsTA, lonsAK, latsTA, latsAK  = QSmapper.getseismometerocations()

		xstationsTA,ystationsTA = self.m(lonsTA,latsTA)

		xstationsAK,ystationsAK = self.m(lonsAK,latsAK)

		self.AKstations = self.m.plot(xstationsAK,ystationsAK,'b^',label='AK',markersize=4,alpha=0.5)

		self.TAstations = self.m.plot(xstationsTA,ystationsTA,'g^',label='TA',markersize=4)

		handles, labels = self.a.get_legend_handles_labels()
		self.a.legend(handles, labels, bbox_to_anchor=(0.2, 0.1), fancybox=True, shadow=True)
		self.canvas.draw()
		self.displayseismometers = True

	def Displayvolcanoes(self):
		'''Display the positions of active volcanoes'''

		if self.displayvolcanoes:

			self.volcanoes.pop(0).remove()

		lats,lons = QSmapper.getvolcanolocations()

		xvolcanoes,yvolcanoes = self.m(lons,lats)
		self.volcanoes = self.m.plot(xvolcanoes,yvolcanoes,'r^',label='Volcanoes',markersize=6)

		handles, labels = self.a.get_legend_handles_labels()
		self.a.legend(handles, labels, bbox_to_anchor=(0.8, 0.1), fancybox=True, shadow=True)
		self.canvas.draw()
		self.displayvolcanoes = True  

	def Displayfaults(self):
		'''Display the locations of majot faults'''

		if self.displayfaults:

			for fault in self.faults:
				fault.pop(0).remove()

			self.faults = []

		lats, lons = QSmapper.getfaultpositions()

		for element in zip(lats,lons):

			faultlats = element[0]
			faultlons = element[1]

			xfaults,yfaults = self.m(faultlons,faultlats)
			fault = self.m.plot(xfaults,yfaults,'b-',linewidth=2)
			self.faults.append(fault)
			self.canvas.draw()

		self.displayfaults = True  


	def Displaynetcdf(self,dataset,label,bounds,colormap):
		'''Generic function for displaying a netcdf dataset'''

		if self.alreadynetcdf:
			self.Resetmap(quick=True)
			self.alreadynetcdf = False

		datasetname = dataset
		data = Dataset(datasetname)

		if 'lon' in data.variables:
			lons = data.variables['lon'][:]
			lats = data.variables['lat'][:]	
		else:
			lons = data.variables['x'][:]
			lats = data.variables['y'][:]				
		
		variable = data.variables['z'][:]

		#create interpolation grid
		nx = int((self.m.xmax-self.m.xmin)/1000.)+1
		ny = int((self.m.ymax-self.m.ymin)/1000.)+1

		#interpolate the nc data over the grid we've just made
		dat = self.m.transform_scalar(variable,lons,lats,nx,ny)

		#create image of the .nc dataset
		image = self.m.imshow(dat,cmap=colormap)
		image.set_clim(bounds[0],bounds[1])
		self.m.drawcoastlines()
		self.m.drawcountries()

		p1 = np.arange(-90.,91.,3.)
		m1 = np.arange(-180.,181.,5.)
		self.m.drawparallels(p1,labels=[False,True,False,False])
		self.m.drawmeridians(m1,labels=[False,False,False,True])

		#Assign colorbar to the correct axis
		self.cbar = plt.colorbar(image,ax=self.a,orientation='horizontal')
		self.cbar.ax.set_xlabel(label)

		self.canvas.draw()

		self.alreadynetcdf = True

	def Displaytomo(self):
		'''Display the crustal thickness data for Alaska. Comes from the Crust 1.0 model'''

		if self.alreadynetcdf:
			self.Resetmap(quick=True)
			self.alreadynetcdf = False

		#load .nc file and extract data
		tomodata = Dataset(self.tomodatasetname)

		lons = tomodata.variables['lon'][:]
		lats = tomodata.variables['lat'][:]
		tomovals = tomodata.variables['z'][:]

		#create interpolation grid
		nx = int((self.m.xmax-self.m.xmin)/1000.)+1
		ny = int((self.m.ymax-self.m.ymin)/1000.)+1

        #interpolate the nc data over the grid we've just made
		tomodata = self.m.transform_scalar(tomovals,lons,lats,nx,ny)

		#create image of the .nc dataset
		image = self.m.imshow(tomodata,cmap=plt.cm.RdBu)
		image.set_clim(-3,3)
		self.m.drawcoastlines()
		self.m.drawcountries()

		p1 = np.arange(-90.,91.,3.)
		m1 = np.arange(-180.,181.,5.)
		self.m.drawparallels(p1,labels=[False,True,False,False])
		self.m.drawmeridians(m1,labels=[False,False,False,True])


		#Assign colorbar to the correct axis
		self.cbar = plt.colorbar(image,ax=self.a,orientation='horizontal')
		self.cbar.ax.set_xlabel('Tomography slice at depth %s km' %str(6371.0-float(self.tomodepthslice)))

		self.canvas.draw()

		self.alreadynetcdf = True	


	def Deleteseis(self):
		'''Remove earthquake and station files in the current directory, so that the program will automatically redownload them in future'''

		os.system('rm %s/Datasets/Earthquakes/All_quakes_Alaska_M*' %myhome)

		print 'Done removing quake dataset!'


	def Resetform(self):
		''''Reset all the entry boxes on the form'''

		print '\n\n#################\n\nForm reset\n\n#################\n\n'

		for entry in self.allentries:
			entry.delete(0,END)


	def Tomoselectionscreen(self):
		'''Creates a new window that allows a user to slice the tomoography model and display map views of interest'''

		if self.Twindowopen == True:
			print 'Error: The "Tomography" window is already open'
			return 
		else:
			self.Twindowopen = True

		#Create pop-up window
		mmtwindow = Toplevel(self)
		mmtwindow.wm_title('Tomography options')

		topmmt=mmtwindow.winfo_toplevel()
		for i in range(180):
			topmmt.rowconfigure(i,weight=1)
			mmtwindow.rowconfigure(i,weight=1)
		for i in range(2):
			topmmt.columnconfigure(i,weight=1)
			mmtwindow.columnconfigure(i,weight=1)

		Label(mmtwindow,text='Use these tools to slice the tomography model and display',bg='powder blue',font='Helvetica 16 bold').grid(row=0,column=0,columnspan=2,ipady=20,sticky=W+E+S+N)

		Label(mmtwindow,text='Dataset name').grid(row=3,column=0,sticky=E+W+S+N,ipady=5)

		eT1 = Entry(mmtwindow)
		eT1.config(width=20)
		eT1.grid(row=3,column=1,sticky=E+W+S+N,ipady=5)
		
		Label(mmtwindow,text='Slice depth [km]').grid(row=4,column=0,sticky=E+W+S+N,ipady=5)

		eT2 = Entry(mmtwindow)
		eT2.config(width=20)
		eT2.grid(row=4,column=1,sticky=E+W+S+N,ipady=5)

		#create the quit button 
		Button(mmtwindow, text='Slice model',width=80,bg='powder blue',command=self.Slicetomo,pady=10).grid(row=5,column=0,columnspan=2,sticky=E+N+W+S,ipady=5)

		Button(mmtwindow, text='Done',width=50,bg='powder blue',command=lambda: self.closechild(mmtwindow,self.Twindowopen)).grid(row=6,column=1,sticky=E,ipady=5)

		self.userentries["Tomodataset"] = eT1
		self.userentries["Tomoslicedepth"] = eT2


	def Slicetomo(self):
		'''Slice a tomography dataset in map view'''

		#Get the datafile that the user entered
		dataset = self.userentries["Tomodataset"].get()

		if os.path.isfile('Datasets/Tomography/'+dataset):
			print 'Extracting tomography from file %s' %dataset

			slicedepth = self.userentries["Tomoslicedepth"].get()

			print slicedepth

			if float(slicedepth) <= 0:
				print 'Slice depth value is invalid'
			else: 

				ncfilename, depth = maketomodepthslice(dataset,float(slicedepth))
				self.tomodatasetname = ncfilename
				self.tomodepthslice = depth
				#call displaytomo function
				self.Displaytomo()

		else:
			print 'Could not find tomography dataset %s' %dataset

	def SaveasPDF(self):
		'''Saves the current frame as a .pdf file'''

		#self.f.savefig('test.pdf',format='pdf')

		filelocation = tkFileDialog.asksaveasfilename(defaultextension='.pdf')
		self.f.savefig(filelocation,format='pdf')
		print 'Saved current figure'

	
	def closechild(self,child,windowname):
		'''Close a child window'''
		child.destroy()
		self.Twindowopen = False



	def Createmenubar(self,parent): 
		'''Create the drop down menu: allows user to add data layers to the Alaska'''

		menubar = Menu(self)
		parent.config(menu=menubar)
		filemenu = Menu(menubar,tearoff=0,font="Helvetica 16 bold") #insert a drop-down menu

		submenu1 = Menu(filemenu)
		submenu1.add_command(label='Active Volcanoes',command=self.Displayvolcanoes)
		submenu1.add_command(label='Faults',command=self.Displayfaults)
		submenu1.add_command(label='Slab contours')
		submenu1.add_command(label='Subducted Yakutat extent',command=self.DisplayYak)
		submenu1.add_command(label='Seismometers',command=self.Displayseismometers)
		submenu1.add_command(label='Last 7 days seismicity (M2.0+)',command=self.DisplayRecentQuakes)
		filemenu.add_cascade(label='Data overlays',menu=submenu1,underline=0)

		filemenu.add_separator()

		submenu2 = Menu(filemenu)
		submenu2.add_command(label='High resolution topo/bathy',command=lambda: self.Displaynetcdf(dataset=myhome+'/Datasets/NetCDF/Alaska_gebco.nc',label='Surface height [m]',bounds=[-5000,5000],colormap=plt.cm.terrain))
		submenu2.add_command(label='Tomography',command=self.Tomoselectionscreen)
		submenu2.add_command(label='Crustal thickness',command=lambda: self.Displaynetcdf(dataset=myhome+'/Datasets/NetCDF/Alaska_crust.nc',label='Crustal thickness [km]',bounds=[0,50],colormap=plt.cm.nipy_spectral))
		submenu2.add_command(label='Gravity anomaly (free air)',command=lambda: self.Displaynetcdf(dataset=myhome+'/Datasets/NetCDF/Alaska_grav_F.nc',label='Free air gravity anomaly [mgal]',bounds=[-160,480],colormap=plt.cm.cubehelix))
		submenu2.add_command(label='Gravity anomaly (bouguer)',command=lambda: self.Displaynetcdf(dataset=myhome+'/Datasets/NetCDF/Alaska_grav_B.nc',label='Bouger gravity anomaly [mgal]',bounds=[-160,480],colormap=plt.cm.cubehelix))
		submenu2.add_command(label='PGE with 10 percent exceedance chance in 50 yrs',command=lambda: self.Displaynetcdf(dataset=myhome+'/Datasets/NetCDF/Alaska_hazard.nc',label='Peak ground acceleration [percent g]',bounds=[0,100],colormap=plt.cm.cubehelix))
		filemenu.add_cascade(label='Data layers',menu=submenu2,underline=0) #add the drop down menu to the menu bar

		filemenu.add_separator() 

		submenu3 = Menu(filemenu)
		submenu3.add_command(label='Save current frame',command=self.SaveasPDF)
		filemenu.add_cascade(label='Other options',menu=submenu3) #add the drop down menu to the menu bar 

		menubar.add_cascade(label="Data options",menu=filemenu)




if __name__ == '__main__':
	'''The GUI loop'''
	tk = Tk()
	viewer = ExplorerGUI(tk)
	tk.mainloop()

